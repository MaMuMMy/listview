import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  final image =[
    AssetImage('asset/image/1.jpg'),
    AssetImage('asset/image/2.jpg'),
    AssetImage('asset/image/3.jpg'),
    AssetImage('asset/image/4.jpg'),
    AssetImage('asset/image/5.jpg'),
    AssetImage('asset/image/5.jpg'),
    AssetImage('asset/image/5.jpg'),
    AssetImage('asset/image/5.jpg'),
    AssetImage('asset/image/5.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView3'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                 backgroundImage: image[index],
                ),
                onTap: (){
                  Fluttertoast.showToast(msg:'${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
                title: Text("${titles[index]}",style: TextStyle(fontSize: 18),),
                subtitle: Text('test test', style: TextStyle(fontSize: 15),),
                trailing: Icon(Icons.notifications_none,size: 25,),
              ),
              Divider(thickness: 1,)
            ],
          );
        },
      ),
    );
  }
}

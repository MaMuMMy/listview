import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListView2"),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 25,
            ),
            title: Text(
              '1.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 25,
            ),
            title: Text(
              '2.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.accessible,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_ferry,
              size: 25,
            ),
            title: Text(
              '3.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.account_balance_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.account_box_rounded,
              size: 25,
            ),
            title: Text(
              '4.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.account_tree_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.adb,
              size: 25,
            ),
            title: Text(
              '5.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.add_a_photo_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.add_chart_rounded,
              size: 25,
            ),
            title: Text(
              '6.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.ac_unit_rounded,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.add_call,
              size: 25,
            ),
            title: Text(
              '7.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.ad_units_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.add_circle,
              size: 25,
            ),
            title: Text(
              '8.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.abc,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.diamond_outlined,
              size: 25,
            ),
            title: Text(
              '9.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.add_alert_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_car_outlined,
              size: 25,
            ),
            title: Text(
              '10.00A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello MaFck',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.directions_bus_outlined,
              size: 25,
            ),
            onTap: (){
              print("MaMu");
            },
          ),
        ],
      ),
    );
  }
}
